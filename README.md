﻿

  

## Hello, Welcome to OVERTIME CALCULATOR

Created by **Akbar Putra Novial 1806173595**


## How to Run?
 1. Download the files from GitLab or clone them by typing              
  `git clone https://gitlab.com/akbarnovial/overtimecalc.git` in your terminal.
 2. Navigate to the path that contains `OvertimeCalculator.jar`
		   
		 ...\OvertimeCalculator\out\artifacts\OvertimeCalculator_jar

 4. Run by typing `java -jar OvertimeCalculator.jar` in your terminal
 5. Start using it :)

## Documentation

 - [Work log](https://docs.google.com/spreadsheets/d/173_vcVz99Zl9yzmbCh05ljWA1xZzq2yS-6HFdVLrGrs/edit?usp=sharing)
 - [Work document](https://docs.google.com/document/d/1w6lsrmrGLO7d3eooy6RACvxMblh5A6O9nubyJ8MMfxU/edit?usp=sharing)


