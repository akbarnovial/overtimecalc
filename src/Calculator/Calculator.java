package Calculator;

import Input.Input;
import Printer.Printer;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;

public class Calculator {


    private static Input input = new Input();
    private static Bonus bonus = new Bonus();

    private static int hourlyRate;
    private static int bonusPay;
    private static int totalPay = 0;
    private static ArrayList<Integer> duration;



    public static int calculateTotalPay() {
        for (int i = 0; i < duration.size(); i++) {
            totalPay += (duration.get(i) * hourlyRate);
        }
        return totalPay + bonus.addBonus();
    }




    public static int getTotalPay() {
        return totalPay;
    }

    public static ArrayList<Integer> getDuration() {
        return duration;
    }

    public static void setDuration(ArrayList<Integer> duration) {
        Calculator.duration = duration;
    }

    public void setTotalPay(int totalPay){
        this.totalPay = totalPay;
    }

    public static Input getInput() {
        return input;
    }

    public void setInput(Input input) {
        this.input = input;
        this.duration = input.getDuration();
        this.hourlyRate = input.getHourlyRate();
        this.bonusPay = input.getBonusPay();
    }
}
