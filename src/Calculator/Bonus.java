package Calculator;

import Input.Input;

public class Bonus {
    private static Input input = new Input();

    private static int daysMonth;
    private static int daysWork;
    private static int bonusPay;
    private static int totalPay;

    public boolean checkAttendance(){
        if (daysMonth == daysWork){
            return true;
        }
        else{
            return false;
        }

    }

    public int addBonus(){
        if(checkAttendance() == true){
            totalPay = bonusPay;

        }
        else{
            totalPay = totalPay;
        }
        return totalPay;
    }

    public void setInput(Input input) {
        this.input = input;
        this.bonusPay = input.getBonusPay();
        this.daysMonth = input.getDaysMonth();
        this.daysWork = input.getDaysWork();
        this.totalPay = input.getTotalPay();
    }
}
