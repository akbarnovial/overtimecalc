package Input;


import Printer.Printer;

import java.io.*;
import java.util.ArrayList;
import java.util.StringTokenizer;

public class Input {

    //Placeholder
    private static int daysMonth;
    private static int daysWork;
    private static int bonusPay;
    private static ArrayList<Integer> duration = new ArrayList<Integer>();
    private static int hourlyRate;
    private static int totalPay = 0;

    public Input() {
    }


    public static int getDaysMonth() {
        return daysMonth;
    }

    public static int getDaysWork() {
        return daysWork;
    }

    public static int getBonusPay() {
        return bonusPay;
    }

    public static ArrayList<Integer> getDuration() {
        return duration;
    }

    public static int getHourlyRate() {
        return hourlyRate;
    }

    public static int getTotalPay() {
        return totalPay;
    }

    public void setDaysMonth(int daysMonth) {
        this.daysMonth = daysMonth;
    }

    public void setDaysWork(int daysWork) {
        this.daysWork = daysWork;
    }

    public void setBonusPay(int bonusPay) {
        this.bonusPay = bonusPay;
    }

    public void setDuration(ArrayList<Integer> duration) {
        this.duration = duration;
    }

    public void setHourlyRate(int hourlyRate) {
        this.hourlyRate = hourlyRate;
    }

    public void setTotalPay(int totalPay) {
        this.totalPay = totalPay;
    }

    public void readInput(Printer printer) throws IOException{
        Input in = new Input(System.in);
        PrintWriter out = new PrintWriter(System.out);

        System.out.println("==============================");
        System.out.println("Hello, Welcome to OvertimeCalc");
        System.out.println("==============================");
        System.out.println();

        System.out.println("How many days are there for your desired month?");
        this.setDaysMonth(in.nextInt());


        System.out.println("How many days did your employee come to work in that month?");
        this.setDaysWork(in.nextInt());

        System.out.println("How much is the hourly rate?");
        this.setHourlyRate(in.nextInt());

        System.out.println("How much is the monthly bonus? You can input 0 if you don't want to give a bonus");
        this.setBonusPay(in.nextInt());

        System.out.println("How long is the duration of the overtime for each day? please input one by one with minutes as the unit, e.g. 1 hour = 60");
        for (int i = 0; i < this.getDaysWork(); i++) {
            this.getDuration().add(in.nextInt());
        }
    }



    public BufferedReader reader;
    public StringTokenizer tokenizer;

    public Input(InputStream stream) {
        reader = new BufferedReader(new InputStreamReader(stream), 32768);
        tokenizer = null;
    }

    public String next() {
        while (tokenizer == null || !tokenizer.hasMoreTokens()) {
            try {
                tokenizer = new StringTokenizer(reader.readLine());
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }
        return tokenizer.nextToken();
    }

    public int nextInt() {
        return Integer.parseInt(next());
    }

    public String nextLine() throws IOException{
        return reader.readLine();
    }

}
