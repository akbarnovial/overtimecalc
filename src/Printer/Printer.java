package Printer;

import Calculator.Calculator;
import Input.Input;
import Calculator.Bonus;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.ArrayList;

public class Printer {
    private static PrintWriter out;
    private static Input input = new Input();
    private static Printer printer = new Printer();
    private static Bonus bonus = new Bonus();
    private static Calculator calculator = new Calculator();
    public static void main(String[] args) throws IOException {

        OutputStream outputStream = System.out;
        out = new PrintWriter(outputStream);
        input.readInput(printer);

        calculator.setInput(input);
        bonus.setInput(input);


        printOvertimePay();

        out.close();
    }

    public static void printOvertimePay(){
        System.out.println("The total you need to pay is " +calculator.calculateTotalPay());
    }
}
